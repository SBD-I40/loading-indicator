# loading-indicator

Polymer Loading indicator circle bounce element.
Fork of the double-bounce loading indicator.

## Dependencies

Element dependencies are managed via [Bower](http://bower.io/). You can
install that via:

    npm install -g bower

Then, go ahead and download the element's dependencies:

    bower install


## Playing With Your Element

If you wish to work on your element in isolation, we recommend that you use
[Polyserve](https://github.com/PolymerLabs/polyserve) to keep your element's
bower dependencies in line. You can install it via:

    npm install -g polyserve

And you can run it via:

    polyserve

Once running, you can preview your element at
`http://localhost:8080/components/loading-indicator/`, where `loading-indicator` is the name of the directory containing it.# loading-indicator

## Usage

Polymer Loading indicator circle bounce element.

Example:
##### Basic usage, hidden by default
    <loading-indicator></loading-indicator>
##### Show the indicator
    <loading-indicator active></loading-indicator>
##### Move the indicator to the right by 50px
    <loading-indicator left-offset="50"></loading-indicator>

### Styling

The following custom properties and mixins are available for styling:

Custom property | Description | Default
----------------|-------------|----------
`--spinner-wrapper-default` | Mixin applied to the spinner when not active | `{}`
`--spinner-wrapper-active` | Mixin applied to the spinner when the spinner is shown | `{}`
`--first-bounce` | Mixin applied to the first circle | `{}`
`--second-bounce` | Mixin applied to the second circle | `{}`
